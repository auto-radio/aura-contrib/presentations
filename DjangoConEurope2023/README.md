# Description

Slides for the talk [The evolution of a Website into a radio automation back-end](https://pretalx.com/djangocon-europe-2023/talk/9MBQEB/) presented remotely at [DjangoCon Europe 2023](https://2023.djangocon.eu/).

The video of the presentation is available on [YouTube](https://www.youtube.com/watch?v=uwjba8FoRVQ)

![](djangocon-europe-2023.png)
