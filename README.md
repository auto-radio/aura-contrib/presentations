# Presentations

Presentations for and about AURA.

## reveal.js

> reveal.js is an open source HTML presentation framework. It's a tool that enables anyone with a web browser to create fully-featured and beautiful presentations for free.

Some presentations are created with reveal.js, in most cases the index.html is provided for this. Read more about [reveal.js](https://revealjs.com).