# Description

Slides for the talk [The evolution of a Website into a radio automation back-end](https://2023.djangocon.us/talks/the-evolution-of-a-django-website-into-a-radio-automation-back-end/) presented remotely at [DjangoCon US 2023](https://2023.djangocon.us/).

![](images/djangocon-us-2023.png)
