# Description

Slides for the talk _AuRa - Overview and Roadmap to the Automated Radio Project_
for the [_danube streamwaves digital_](https://www.freefm.de/projekte/danubestreamwaves-digital)
project.

# How to set up

Besides _index.html_, _local.css_ and the files in the _imgs_ folder you need
[reveal.js](https://revealjs.com/). This can be cloned from their
[repository](https://github.com/hakimel/reveal.js) or you download it directly
as a [release tarball](https://github.com/hakimel/reveal.js/releases),
in this case for version 4.1.0:

```bash
wget https://github.com/hakimel/reveal.js/archive/4.1.0.tar.gz
```

Then unpack it and create a `reveal.js` link to the current version:

```bash
tar xzf 4.1.0.tar.gz
ln -s reveal.js-4.1.0 reveal.js
```

Ok, you are done now. Just open the _index.html_ in a web browser.

# Deployment on a webspace

The setup above can be done either locally or on remote and web accessible
directory.

If you don't want to clone the whole repository, just use the following
procedure:

```bash
wget https://gitlab.servus.at/aura/aura/-/archive/main/meta-main.tar.gz?path=presentations/dsw2020
tar xzf dsw2020.tar.gz
rm dsw2020.tar.gz
mv dsw2020/* .
rm -r dsw2020
wget https://github.com/hakimel/reveal.js/archive/4.1.0.tar.gz
tar xzf 4.1.0.tar.gz
rm 4.1.0.tar.gz
ln -s reveal.js-4.1.0 reveal.js
```
