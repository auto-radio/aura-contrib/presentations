# Description

Folien für den Vortrag _AuRa - Überblick und Ausblick zum Automated Radio Projekt_
bei der [_Zukunftswerkstatt Community Media 2020_](https://www.community-media.net/).

# How to set up

Neben der _index.html_, _local.css_ und den Files im _imgs_ Folder wird noch
[reveal.js](https://revealjs.com/) benötigt. Dieses kann aus deren
[repository](https://github.com/hakimel/reveal.js) geklont, oder
direkt als [release tarball](https://github.com/hakimel/reveal.js/releases)
heruntergelanden werden, hier z.B. für Version 4.1.0:

```bash
wget https://github.com/hakimel/reveal.js/archive/4.1.0.tar.gz
```

Dann entpacken wir das reveal.js-Paket und legen einen Link von `reveal.js` zur
aktuellen Version an, in unserem Beispiel 4.1.0:

```bash
tar xzf 4.1.0.tar.gz
ln -s reveal.js-4.1.0 reveal.js
```

Ok, alles paletti. Nun braucht mensch nur noch die _index.html_ in einem Browser
zu öffnen.

# Deployment auf einem Webspace

Das oben beschriebene Setup kann sowohl lokal als auch auf einem remote bzw.
web accessible directory vorgenommen werden.

Um nicht extra das ganze Repo zu klonen kann folgender Ablauf verwendet werden:

```bash
wget https://gitlab.servus.at/aura/aura/-/archive/main/meta-main.tar.gz?path=presentations/zwcm2020
tar xzf zwcm2020.tar.gz
rm zwcm2020.tar.gz
mv zwcm2020/* .
rm -r zwcm2020
wget https://github.com/hakimel/reveal.js/archive/4.1.0.tar.gz
tar xzf 4.1.0.tar.gz
rm 4.1.0.tar.gz
ln -s reveal.js-4.1.0 reveal.js
```
